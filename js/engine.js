$(function() {


// Фиксированная шапка припрокрутке
$(window).scroll(function(){

  if ($(window).width() > 768) {
      var sticky = $('.header'),
          scroll = $(window).scrollTop();
      if (scroll > 100) {
          sticky.addClass('header-fixed');
      } else {
          sticky.removeClass('header-fixed');
      };
    }

   
});


$(window).resize(function() {
  if ($(window).width() <= 768) {
      $('.header').removeClass('header-fixed'); 
    }

});






// Banner slider
  $('.banner-slider').slick({
      infinite: true,
      slidesToShow: 1,
      slidesToScroll: 1,
      autoplay: true,
      autoplaySpeed:4000,
      fade: false,
      arrows: false,
      dots: false,
      centerMode: true,
      centerPadding: 0,
      variableWidth: true,
      responsive: [{
        breakpoint: 1200,
        settings: {
            variableWidth: false,
        }
        
      }
    ]
     
  });



// Фиксированный блок на странице Оформление заказа
if ($(window).width() > 992) {
      $('.airSticky').airStickyBlock();
};




// FansyBox
 $('.fancybox').fancybox({
  closeExisting: true,
 });


// Маска телефона
$(".phone-mask").mask("+7 (999) 99-99-999");

// Календарь
$(function () {
    $('#datetimepicker').datetimepicker({
        locale: 'ru',
        format: 'DD/MM/YYYY'

    });
});
// Время доставки
$(function () {
    $('#timepicker').datetimepicker({
        locale: 'ru',
        format: 'HH:mm'

    });
});


// Показать\скрыть историю заказов
$('.history-item__header').click(function() {
  $(this).parent().toggleClass('active');

})




// Переключатель
$('.switch-btn').click(function (e, changeState) {
    if (changeState === undefined) {
        $(this).toggleClass('switch-on');
    }
    if ($(this).hasClass('switch-on')) {
        $(this).trigger('on.switch');
    } else {
        $(this).trigger('off.switch');
    }
});



// Новый адрес доставки

$('input:radio[name="delivery-group"]').change(
    function(){
        if ($('#delivery-new').is(':checked')) {
            $('.delivery-new').fadeIn(100)
        } else {
          $('.delivery-new').fadeOut(100)
        }
    });

// Добавить комментарий к адресу доставки
$('.delivery-new__address .switch-btn').click(function() {
  if($(this).hasClass('switch-on')){
    $('.delivery-new__address .form-field').fadeIn(100);
  }else{
    $('.delivery-new__address .form-field').fadeOut(100);
  }
})


// Begin of slider-range
$( "#slider-range" ).slider({
  range: 'min',
  min: 0,
  max: 500,
  value: 150,
  slide: function( event, ui ) {
    $( "#amount" ).text(ui.value);
  }
});
$( "#amount" ).val( $( "#slider-range" ).slider( "value" ) );

// END of slider-range




 // Стилизация селектов
$('select').styler();


// Табы
$('.tabs-nav a').click(function() {
    var _id = $(this).attr('href');
    var _targetElement = $(this).parents('.tabs-wrap').find('#' + _id);
    $(this).parents('.tabs-nav').find('a').removeClass('active');
    $(this).addClass('active');
     $(this).parents('.tabs-wrap').find('.tabs-item').removeClass('active');
    _targetElement.addClass('active');
    return false;

});


// сайдбар на мобильном
$('.sidebar-mobile__btn').click(function() {
  $(this).toggleClass('active');
  $('.sidebar').toggleClass('active')
})



// MMENU
if ($(window).width() < 1200) {
var menu = new MmenuLight(
  document.querySelector( '#mobile-menu' ),
  'all'
);

var navigator = menu.navigation({
  // selectedClass: 'Selected',
  // slidingSubmenus: true,
  // theme: 'dark',
   title: 'Меню'
});

var drawer = menu.offcanvas({
  // position: 'left'
});

//  Open the menu.
document.querySelector( 'a[href="#mobile-menu"]' )
  .addEventListener( 'click', evnt => {
    evnt.preventDefault();
    drawer.open();
});


}




})